package com.tech.test.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.net.SocketFactory;

@Configuration
public class FeedMeConfiguration {

    @Bean
    public SocketFactory defaultSocketFactory() {
        return SocketFactory.getDefault();
    }
}
