package com.tech.test.services;

public interface FeedmeService {

    /**
     * Read fixtures from feedme provider
     * @return int - returned when no more messages received
     */
    int getFixtureMessageData();

}
