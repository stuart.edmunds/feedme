package com.tech.test.services;

import java.io.IOException;

public interface EventProviderService {

    /**
     * Reads a message from the Feedme Provider service
     *
     * @return String - The proprietary data message
     * @throws IOException
     */
    String readNextMessage() throws IOException;
}
