package com.tech.test.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.net.SocketFactory;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

@Service
public class EventProviderServiceImpl implements EventProviderService {

    private static final Logger LOG = LoggerFactory.getLogger(EventProviderServiceImpl.class);

    private SocketFactory defaultSocketFactory;
    private Environment env;
    private Socket socket;
    private BufferedReader in;

    @Autowired
    public EventProviderServiceImpl(SocketFactory defaultSocketFactory, Environment env) {
        this.defaultSocketFactory = defaultSocketFactory;
        this.env = env;
    }

    @Override
    public String readNextMessage() throws IOException {
        if (this.in == null)
            initialise();

        return this.in.readLine();
    }

    protected void initialise() throws IOException {
        createSocket();
        this.in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    }

    private void createSocket() throws IOException {
        this.socket = defaultSocketFactory.createSocket(getServiceEndpoint(), getServiceEndpointPort());
    }

    private String getServiceEndpoint()
    {
        return env.getProperty("hostname", "localhost");
    }

    private int getServiceEndpointPort()
    {
        return Integer.parseInt(env.getProperty("port", "8282"));
    }

}
