package com.tech.test.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tech.test.dtos.FixtureDto;
import com.tech.test.dtos.MarketDto;
import com.tech.test.dtos.OutcomeDto;
import com.tech.test.dtos.mappers.FixtureDtoMapper;
import com.tech.test.dtos.mappers.MarketDtoMapper;
import com.tech.test.dtos.mappers.OutcomeDtoMapper;
import com.tech.test.models.Event;
import com.tech.test.models.Market;
import com.tech.test.models.Outcome;
import com.tech.test.models.PacketType;
import com.tech.test.models.mappers.EventMapper;
import com.tech.test.models.mappers.MarketMapper;
import com.tech.test.models.mappers.OutcomeMapper;
import com.tech.test.repositories.FixtureRepository;
import com.tech.test.repositories.entries.FixtureEntry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
public class FeedmeServiceImpl implements FeedmeService {

    public static final String SPLIT_PROPRIETARY_MESSAGE_REGEX = "^\\||(?<=[^\\\\])\\|";

    private static Logger LOG = LoggerFactory.getLogger(FeedmeServiceImpl.class);
    private static final int PACKET_TYPE_INDEX = 2;
    private static final int SUCCESS = 0;

    private EventProviderService eventProviderService;
    private Environment env;
    private FixtureRepository repository;

    @Autowired
    public FeedmeServiceImpl(EventProviderService eventProviderService, Environment env, FixtureRepository repository) {
        this.eventProviderService = eventProviderService;
        this.env = env;
        this.repository = repository;
    }

    @Override
    public int getFixtureMessageData() {

        List<String> proprietaryFixtureMessages = null;
        String[] tokens;
        String raw;
        while ((raw = getNextMessage()) != null) {
            tokens = raw.split(SPLIT_PROPRIETARY_MESSAGE_REGEX);
            tokens = Arrays.copyOfRange(tokens, 1, tokens.length);
            if (PacketType.EVENT.equals(PacketType.getType(tokens[PACKET_TYPE_INDEX]))) {
                // Received all event/fixture and associated messages so persist it
                if (proprietaryFixtureMessages != null) {
                    insertFixtureDto(processMessages(proprietaryFixtureMessages));
                    proprietaryFixtureMessages = null;
                }
                proprietaryFixtureMessages = new ArrayList<>();
                proprietaryFixtureMessages.add(convertMessageToJSON(tokens));

            }
            else {
                if (proprietaryFixtureMessages != null) {
                    proprietaryFixtureMessages.add(convertMessageToJSON(tokens));
                }
            }
        }
        /* process last proprietary fixture messages */
        if (proprietaryFixtureMessages != null) {
            processMessages(proprietaryFixtureMessages);
        }

        return SUCCESS;
    }



    /**
     * Process all the Event/Fixture messages (Event, Markets & Outcome)
     *
     * @param fixtureMessagesAsJson
     * @return FixtureDto -
     */
    protected FixtureDto processMessages(List<String> fixtureMessagesAsJson) {
        try {
            FixtureDto fixtureDto = null;
            MarketDto marketDto = null;
            for(String eachMsg : fixtureMessagesAsJson) {

                PacketType type = getPacketType(eachMsg);

                if (PacketType.EVENT.equals(type)) {
                    LOG.debug("Processing EVENT message={}", eachMsg);
                    Event event = new ObjectMapper().readValue(eachMsg, Event.class);
                    fixtureDto = FixtureDtoMapper.EventToFixtureDtoMapper(event);

                } else if (PacketType.MARKET.equals(type) && fixtureDto != null) {
                    LOG.debug("Processing MARKET message={}", eachMsg);
                    Market market = new ObjectMapper().readValue(eachMsg, Market.class);
                    marketDto = MarketDtoMapper.MarketToMarketDtoMapper(market);
                    fixtureDto.getMarketDto().add(marketDto);

                } else if (PacketType.OUTCOME.equals(type) && fixtureDto != null) {
                    LOG.debug("Processing OUTCOME message={}", eachMsg);
                    Outcome outcome = new ObjectMapper().readValue(eachMsg, Outcome.class);
                    OutcomeDto outcomeDto = OutcomeDtoMapper.outcomeToOutcomeDtoMapper(outcome);
                    marketDto.getOutComeDto().add(outcomeDto);
                }
            }
            return fixtureDto;
        } catch (IOException e) {
            LOG.error("Failed to process message",e);
           throw new RuntimeException("Unexpected data received", e);
        }
    }

    /**
     * Determine the packetType
     *
     * @param json
     * @return PacketType
     */
    protected PacketType getPacketType(String json) {

        try {
            JsonNode root = new ObjectMapper().readTree(json);
            JsonNode nameNode = root.path("header");
            return PacketType.getType(nameNode.path("type").asText());

        } catch (IOException e) {
            LOG.error("Failed to parse JSON and extract packageType. JSON={}",json,e);
            throw new RuntimeException("Failed to parse JSON and extract packageType. JSON="+json, e);
        }
    }

    /**
     * Convert the tokens[] into the relevant JSON
     *
     * @param tokens - proprietary msg broken into tokens[]
     * @return String
     */
    protected String convertMessageToJSON(String[] tokens) {
        try {
            switch(PacketType.getType(tokens[PACKET_TYPE_INDEX])) {
                case EVENT :
                    Event event =  EventMapper.proprietaryDataToEventMapper(tokens);
                    return new ObjectMapper().writeValueAsString(event);
                case MARKET :
                    Market market = MarketMapper.proprietaryDataToMarketMapper(tokens);
                    return new ObjectMapper().writeValueAsString(market);
                case OUTCOME:
                    Outcome outcome = OutcomeMapper.proprietaryDataToOutcomeMapper(tokens);
                    return new ObjectMapper().writeValueAsString(outcome);
                default:
                    LOG.error("Packet Type was NOT recognised!");
                    return ""; // TODO may be throw exception?
            }
        } catch (JsonProcessingException e) {
            LOG.error("Failed to convert raw message into JSON",e);
            throw new RuntimeException("Failed to convert raw message into JSON", e);
        }
    }

    /**
     * Reads the next message
     * @return String - proprietary message format
     */
    protected  String getNextMessage() {
        try {
            return eventProviderService.readNextMessage();
        } catch (IOException e) {
            LOG.error("Failed to read next message from Feedme Provider Service", e);
            throw new RuntimeException("Failed to read next message from Feedme Provider Service", e);
        }
    }

    /**
     * Persist FixtureDto into DB
     *
     * @param fixtureDto
     */
    protected void insertFixtureDto(FixtureDto fixtureDto)
    {
        Assert.notNull(fixtureDto, "The supplied Collection of fixtureDto's must not be null");

        LOG.debug("Attempting to save a new FixtureEntries");

        // Convert each FixtureDto into FixtureEntry ready to be inserted into DB
        FixtureEntry fixtureEntry = createFixtureDto(fixtureDto);

        // Store FixtureEntry in DB
        repository.save(fixtureEntry);
    }

    /**
     * Create the FixtureEntry from the parameter fixtureDto ready to be persisted
     * @param fixtureDto
     * @return FixtureEntry
     */
    protected FixtureEntry createFixtureDto(FixtureDto fixtureDto) {
        Assert.notNull(fixtureDto.getEventId(), "EventId must not be null");

        Date now = Date.from(Instant.now());

        FixtureEntry fixtureEntry = FixtureEntry.getBuilder()
            .setWhenCreated(now)
            .setLastUpdated(now)
            ._id(fixtureDto.getEventId() + "-" + fixtureDto.getHeaderDto().getType())
            .setEventId(fixtureDto.getEventId())
            .setCategory(fixtureDto.getCategory())
            .setSubCategory(fixtureDto.getSubCategory())
            .setName(fixtureDto.getName())
            .setStartTime(fixtureDto.getStartTime())
            .setDisplayed(fixtureDto.isDisplayed())
            .setSuspended(fixtureDto.isSuspended())
            .setHeaderDto(fixtureDto.getHeaderDto())
            .setMarketDto(fixtureDto.getMarketDto())
            .builder();

        LOG.debug("FixtureEntry={}", fixtureEntry);
        return fixtureEntry;
    }

}
