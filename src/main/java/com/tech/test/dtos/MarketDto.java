package com.tech.test.dtos;

import com.tech.test.models.Header;

import java.util.List;

public class MarketDto {

    private HeaderDto headerDto;

    private String eventId;
    private String marketId;
    private String name;
    private boolean displayed;
    private boolean suspended;

    private List<OutcomeDto> outComeDto;

    public HeaderDto getHeaderDto() {
        return headerDto;
    }

    public void setHeaderDto(HeaderDto headerDto) {
        this.headerDto = headerDto;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getMarketId() {
        return marketId;
    }

    public void setMarketId(String marketId) {
        this.marketId = marketId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDisplayed() {
        return displayed;
    }

    public void setDisplayed(boolean displayed) {
        this.displayed = displayed;
    }

    public boolean isSuspended() {
        return suspended;
    }

    public void setSuspended(boolean suspended) {
        this.suspended = suspended;
    }

    public List<OutcomeDto> getOutComeDto() {
        return outComeDto;
    }

    public void setOutComeDto(List<OutcomeDto> outComeDto) {
        this.outComeDto = outComeDto;
    }
}
