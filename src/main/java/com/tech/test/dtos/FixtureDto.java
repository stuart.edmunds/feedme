package com.tech.test.dtos;

import java.util.List;

public class FixtureDto {
    private HeaderDto headerDto;

    private String eventId;
    private String category;
    private String subCategory;
    private String name;
    private long startTime;
    private boolean displayed;
    private boolean suspended;

    private List<MarketDto> marketDto;

    public HeaderDto getHeaderDto() {
        return headerDto;
    }

    public void setHeaderDto(HeaderDto headerDto) {
        this.headerDto = headerDto;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public boolean isDisplayed() {
        return displayed;
    }

    public void setDisplayed(boolean displayed) {
        this.displayed = displayed;
    }

    public boolean isSuspended() {
        return suspended;
    }

    public void setSuspended(boolean suspended) {
        this.suspended = suspended;
    }

    public List<MarketDto> getMarketDto() {
        return marketDto;
    }

    public void setMarketDto(List<MarketDto> marketDto) {
        this.marketDto = marketDto;
    }
}
