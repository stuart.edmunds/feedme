package com.tech.test.dtos;

import java.util.Date;

public class FixtureEntryDto {
    private final String context, eventId, value;

    private final Date whenCreated, lastUpdated;

    public String getContext() {
        return context;
    }

    public String getEventId() {
        return eventId;
    }

    public String getValue() {
        return value;
    }

    public Date getWhenCreated() {
        return whenCreated;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public FixtureEntryDto(String context, String eventId, String value, Date whenCreated, Date lastUpdated)
    {
        this.context = context;
        this.eventId = eventId;
        this.value = value;
        this.whenCreated = whenCreated;
        this.lastUpdated = lastUpdated;
    }

    @Override
    public String toString() {
        return "FixtureEntryDto{" +
            "context='" + context + '\'' +
            ", eventId='" + eventId + '\'' +
            ", value='" + value + '\'' +
            ", whenCreated=" + whenCreated +
            ", lastUpdated=" + lastUpdated +
            '}';
    }
}
