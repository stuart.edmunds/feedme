package com.tech.test.dtos.mappers;

import com.tech.test.dtos.OutcomeDto;
import com.tech.test.models.Outcome;
import com.tech.test.models.mappers.HeaderMapper;

public class OutcomeDtoMapper {

    public static OutcomeDto outcomeToOutcomeDtoMapper(Outcome outcome) {

        OutcomeDto outcomeDto = new OutcomeDto();
        outcomeDto.setHeaderDto(HeaderDtoMapper.headerToHeaderDtoMapper(outcome.getHeader()));
        outcomeDto.setMarketId(outcome.getMarketId());
        outcomeDto.setOutcomeId(outcome.getOutcomeId());
        outcomeDto.setName(outcome.getName());
        outcomeDto.setPrice(outcome.getPrice());
        outcomeDto.setDisplayed(outcome.isDisplayed());
        outcomeDto.setSuspended(outcome.isSuspended());

        return outcomeDto;
    }

}
