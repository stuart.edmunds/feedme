package com.tech.test.dtos.mappers;

import com.tech.test.dtos.MarketDto;
import com.tech.test.models.Market;
import com.tech.test.models.mappers.HeaderMapper;

import java.util.ArrayList;

public class MarketDtoMapper {

    public static MarketDto MarketToMarketDtoMapper(Market market) {

        MarketDto marketDto = new MarketDto();
        marketDto.setHeaderDto(HeaderDtoMapper.headerToHeaderDtoMapper(market.getHeader()));
        marketDto.setEventId(market.getEventId());
        marketDto.setMarketId(market.getMarketId());
        marketDto.setName(market.getName());
        marketDto.setDisplayed(market.isDisplayed());
        marketDto.setSuspended(market.isSuspended());

        marketDto.setOutComeDto(new ArrayList<>());
        return marketDto;
    }
}
