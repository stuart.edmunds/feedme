package com.tech.test.dtos.mappers;

import com.tech.test.dtos.HeaderDto;
import com.tech.test.models.Header;

public class HeaderDtoMapper {

    public static Header proprietaryDataToHeaderMapper(String[] tokens) {
        Header header = new Header();
        header.setMsgId(Integer.parseInt(tokens[0]));
        header.setOperation(tokens[1]);
        header.setType(tokens[2]);
        header.setTimestamp(Long.parseLong(tokens[3]));

        return header;
    }

    public static HeaderDto headerToHeaderDtoMapper(Header header) {
        HeaderDto headerDto = new HeaderDto();
        headerDto.setMsgId(header.getMsgId());
        headerDto.setOperation(header.getOperation());
        headerDto.setType(header.getType());
        headerDto.setTimestamp(header.getTimestamp());

        return headerDto;
    }
}
