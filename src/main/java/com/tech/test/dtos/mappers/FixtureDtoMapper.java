package com.tech.test.dtos.mappers;

import com.tech.test.dtos.FixtureDto;
import com.tech.test.models.Event;

import java.util.ArrayList;

public class FixtureDtoMapper {

    public static FixtureDto EventToFixtureDtoMapper(Event event) {

        FixtureDto fixtureDto = new FixtureDto();
        fixtureDto.setHeaderDto(HeaderDtoMapper.headerToHeaderDtoMapper(event.getHeader()));
        fixtureDto.setEventId(event.getEventId());
        fixtureDto.setCategory(event.getCategory());
        fixtureDto.setSubCategory(event.getSubCategory());
        fixtureDto.setName(event.getName());
        fixtureDto.setStartTime(event.getStartTime());
        fixtureDto.setDisplayed(event.isDisplayed());
        fixtureDto.setSuspended(event.isSuspended());

        fixtureDto.setMarketDto(new ArrayList<>());

        return fixtureDto;
    }
}
