package com.tech.test.repositories;

import com.tech.test.dtos.FixtureEntryDto;
import com.tech.test.repositories.entries.FixtureEntry;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface FixtureRepository extends MongoRepository<FixtureEntry, String> {

    List<FixtureEntryDto> findByEventId(String context);

}
