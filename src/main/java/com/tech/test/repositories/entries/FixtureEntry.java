package com.tech.test.repositories.entries;

import com.tech.test.dtos.HeaderDto;
import com.tech.test.dtos.MarketDto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class FixtureEntry implements Serializable {
    private static final long serialVersionUID = -2059372448996511873L;
    private String _id;

    private Date whenCreated;

    private Date lastUpdated;

    private HeaderDto headerDto;

    private String eventId;
    private String category;
    private String subCategory;
    private String name;
    private long startTime;
    private boolean displayed;
    private boolean suspended;

    private List<MarketDto> marketDto;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public Date getWhenCreated() {
        return whenCreated;
    }

    public void setWhenCreated(Date whenCreated) {
        this.whenCreated = whenCreated;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public HeaderDto getHeaderDto() {
        return headerDto;
    }

    public void setHeaderDto(HeaderDto headerDto) {
        this.headerDto = headerDto;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public boolean isDisplayed() {
        return displayed;
    }

    public void setDisplayed(boolean displayed) {
        this.displayed = displayed;
    }

    public boolean isSuspended() {
        return suspended;
    }

    public void setSuspended(boolean suspended) {
        this.suspended = suspended;
    }

    public List<MarketDto> getMarketDto() {
        return marketDto;
    }

    public void setMarketDto(List<MarketDto> marketDto) {
        this.marketDto = marketDto;
    }

    /**
     * Required by Hibernate
     */

    public FixtureEntry() {
    }

    private FixtureEntry(Builder builder) {
        this._id = builder._id;
        this.whenCreated = builder.whenCreated;
        this.lastUpdated = builder.lastUpdated;
        this.headerDto = builder.headerDto;
        this.eventId = builder.eventId;
        this.category = builder.category;
        this.subCategory = builder.subCategory;
        this.name = builder.name;
        this.startTime = builder.startTime;
        this.displayed = builder.displayed;
        this.suspended = builder.suspended;
        this.marketDto = builder.marketDto;


    }

    public static Builder getBuilder() {
        return new Builder();
    }

    /**
     * Builds an instance of ConfigEntry
     */
    public static class Builder {
        private String _id;
        private Date whenCreated;
        private Date lastUpdated;
        private HeaderDto headerDto;
        private String eventId;
        private String category;
        private String subCategory;
        private String name;
        private long startTime;
        private boolean displayed;
        private boolean suspended;
        private List<MarketDto> marketDto;

        private Builder() {
        }

        public Builder _id(String _id) {
            this._id = _id;
            return this;
        }

        public Builder setWhenCreated(Date whenCreated) {
            this.whenCreated = whenCreated;
            return this;
        }

        public Builder setLastUpdated(Date lastUpdated) {
            this.lastUpdated = lastUpdated;
            return this;
        }

        public Builder setHeaderDto(HeaderDto headerDto) {
            this.headerDto = headerDto;
            return this;
        }
        public Builder setEventId(String eventId) {
            this.eventId = eventId;
            return this;
        }
        public Builder setCategory(String category) {
            this.category = category;
            return this;
        }
        public Builder setSubCategory(String subCategory) {
            this.subCategory = subCategory;
            return this;
        }
        public Builder setName(String name) {
            this.name = name;
            return this;
        }
        public Builder setStartTime(long startTime) {
            this.startTime = startTime;
            return this;
        }
        public Builder setDisplayed(boolean displayed) {
            this.displayed = displayed;
            return this;
        }
        public Builder setSuspended(boolean suspended) {
            this.suspended = suspended;
            return this;
        }
        public Builder setMarketDto(List<MarketDto> marketDto) {
            this.marketDto = marketDto;
            return this;
        }

        public FixtureEntry builder() {
            FixtureEntry build = new FixtureEntry(this);
            // TODO Add additional validation checks here
            return build;
        }

        @Override
        public String toString() {
            return "Builder{" +
                "_id='" + _id + '\'' +
                ", whenCreated=" + whenCreated +
                ", lastUpdated=" + lastUpdated +
                ", headerDto=" + headerDto +
                ", eventId='" + eventId + '\'' +
                ", category='" + category + '\'' +
                ", subCategory='" + subCategory + '\'' +
                ", name='" + name + '\'' +
                ", startTime=" + startTime +
                ", displayed=" + displayed +
                ", suspended=" + suspended +
                ", marketDto=" + marketDto +
                '}';
        }
    }
}
