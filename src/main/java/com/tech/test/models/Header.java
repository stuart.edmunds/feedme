package com.tech.test.models;

import java.io.Serializable;

/**
 * <header>
 *      <field index="0" name="msgId" datatype="integer"/>
 *      <field index="1" name="operation" datatype="string"/>
 *      <field index="2" name="type" datatype="string"/>
 *      <field index="3" name="timestamp" datatype="integer"/>
 * </header>
 *
 * Example: |326|update|outcome|1556222116206|
 */
public class Header implements Serializable {

    private static final long serialVersionUID = -7075855164717156623L;

    private int msgId;
    private String operation;
    private String type;
    private long timestamp;

    public Header() {
    }

    public int getMsgId() {
        return msgId;
    }

    public void setMsgId(int msgId) {
        this.msgId = msgId;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
