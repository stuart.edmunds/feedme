package com.tech.test.models;

import java.util.HashMap;
import java.util.Map;

public enum OperationType {
    CREATE("create"), UPDATE("update");

    private String value;
    private static Map<String, OperationType> map = new HashMap<>();

    static {
        for (OperationType entry : OperationType.values()) {
            map.put(entry.value, entry);
        }
    }

    OperationType(final String value) {
        this.value = value;
    }

    public static OperationType getType(String type) {

        if (map.containsKey(type)) {
            return map.get(type);
        }

        return null;
    }
}
