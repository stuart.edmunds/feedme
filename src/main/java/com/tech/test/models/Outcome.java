package com.tech.test.models;

import java.io.Serializable;

/**
 * <body>
 *      <field index="4" name="marketId" datatype="string"/>
 *      <field index="5" name="outcomeId" datatype="string"/>
 *      <field index="6" name="name" datatype="string"/>
 *      <field index="7" name="price" datatype="string"/>
 *      <field index="8" name="displayed" datatype="boolean"/>
 *      <field index="9" name="suspended" datatype="boolean"/>
 * </body>
 */
public class Outcome implements Serializable {

    private static final long serialVersionUID = 8172953636235972788L;

    private Header header;

    private String marketId;
    private String outcomeId;
    private String name;
    private String price;
    private boolean displayed;
    private  boolean suspended;

    public Outcome() {
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public String getMarketId() {
        return marketId;
    }

    public void setMarketId(String marketId) {
        this.marketId = marketId;
    }

    public String getOutcomeId() {
        return outcomeId;
    }

    public void setOutcomeId(String outcomeId) {
        this.outcomeId = outcomeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public boolean isDisplayed() {
        return displayed;
    }

    public void setDisplayed(boolean displayed) {
        this.displayed = displayed;
    }

    public boolean isSuspended() {
        return suspended;
    }

    public void setSuspended(boolean suspended) {
        this.suspended = suspended;
    }

    //    public String toJSONString() {
//        return Json.createObjectBuilder()
//            // Header
//            .addAll(header.toJSONString())
//            // Outcome Body
//            .add("marketId", marketId)
//            .add("outcomeId", outcomeId)
//            .add("name", name)
//            .add("price", price)
//            .add("displayed", displayed)
//            .add("suspended", suspended)
//            .build()
//            .toString();
//    }
}
