package com.tech.test.models;

import java.io.Serializable;

/**
 * <body>
 *      <field index="4" name="eventId" datatype="string"/>
 *      <field index="5" name="marketId" datatype="string"/>
 *      <field index="6" name="name" datatype="string"/>
 *      <field index="7" name="displayed" datatype="boolean"/>
 *      <field index="8" name="suspended" datatype="boolean"/>
 * </body>
 */
public class Market implements Serializable {
    private Header header;

    private String eventId;
    private String marketId;
    private String name;
    private boolean displayed;
    private boolean suspended;

    public Market() {
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getMarketId() {
        return marketId;
    }

    public void setMarketId(String marketId) {
        this.marketId = marketId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDisplayed() {
        return displayed;
    }

    public void setDisplayed(boolean displayed) {
        this.displayed = displayed;
    }

    public boolean isSuspended() {
        return suspended;
    }

    public void setSuspended(boolean suspended) {
        this.suspended = suspended;
    }

    //    public String toJSONString() {
//        return Json.createObjectBuilder()
//            // Header
//            .addAll(header.toJSONString())
//            // Market Body
//            .add("eventId", eventId)
//            .add("marketId", marketId)
//            .add("name", name)
//            .add("displayed", displayed)
//            .add("suspended", suspended)
//            .build()
//            .toString();
//    }
}
