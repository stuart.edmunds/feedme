package com.tech.test.models;

import java.io.Serializable;

/**
 * <body>
 *      <field index="4" name="eventId" datatype="string"/>
 *      <field index="5" name="category" datatype="string"/>
 *      <field index="6" name="subCategory" datatype="string"/>
 *      <field index="7" name="name" datatype="string"/>
 *      <field index="8" name="startTime" datatype="integer"/>
 *      <field index="9" name="displayed" datatype="boolean"/>
 *      <field index="10" name="suspended" datatype="boolean"/>
 * </body>
 */
public class Event implements Serializable {

    private static final long serialVersionUID = 8235655094854303121L;

    private Header header;

    private String eventId;
    private String category;
    private String subCategory;
    private String name;
    private long startTime;
    private boolean displayed;
    private boolean suspended;

    public Event() { }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public boolean isDisplayed() {
        return displayed;
    }

    public void setDisplayed(boolean displayed) {
        this.displayed = displayed;
    }

    public boolean isSuspended() {
        return suspended;
    }

    public void setSuspended(boolean suspended) {
        this.suspended = suspended;
    }

    @Override
    public String toString() {
        return "Event{" +
            "header=" + header +
            ", eventId='" + eventId + '\'' +
            ", category='" + category + '\'' +
            ", subCategory='" + subCategory + '\'' +
            ", name='" + name + '\'' +
            ", startTime=" + startTime +
            ", displayed=" + displayed +
            ", suspended=" + suspended +
            '}';
    }
}
