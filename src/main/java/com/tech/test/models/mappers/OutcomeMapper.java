package com.tech.test.models.mappers;

import com.tech.test.models.Outcome;

public class OutcomeMapper {

    public static Outcome proprietaryDataToOutcomeMapper(String[] tokens) {

        Outcome outcome = new Outcome();
        outcome.setHeader(HeaderMapper.proprietaryDataToHeaderMapper(tokens));
        outcome.setMarketId(tokens[4]);
        outcome.setOutcomeId(tokens[5]);
        outcome.setName(tokens[6]);
        outcome.setPrice(tokens[7]);
        outcome.setDisplayed(Boolean.parseBoolean(tokens[8]));
        outcome.setSuspended(Boolean.parseBoolean(tokens[9]));

        return outcome;
    }
}
