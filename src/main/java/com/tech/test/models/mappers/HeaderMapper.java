package com.tech.test.models.mappers;

import com.tech.test.models.Header;

public class HeaderMapper {

    public static Header proprietaryDataToHeaderMapper(String[] tokens) {
        Header header = new Header();
        header.setMsgId(Integer.parseInt(tokens[0]));
        header.setOperation(tokens[1]);
        header.setType(tokens[2]);
        header.setTimestamp(Long.parseLong(tokens[3]));

        return header;
    }

}
