package com.tech.test.models.mappers;

import com.tech.test.models.Event;

public class EventMapper {

    public static Event proprietaryDataToEventMapper(String[] tokens) {

        Event event = new Event();
        event.setHeader(HeaderMapper.proprietaryDataToHeaderMapper(tokens));
        event.setEventId(tokens[4]);
        event.setCategory(tokens[5]);
        event.setSubCategory(tokens[6]);
        event.setName(tokens[7]);
        event.setStartTime(Long.parseLong(tokens[8]));
        event.setDisplayed(Boolean.parseBoolean(tokens[9]));
        event.setSuspended(Boolean.parseBoolean(tokens[10]));

        return event;
    }
}
