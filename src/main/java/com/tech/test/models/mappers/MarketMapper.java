package com.tech.test.models.mappers;

import com.tech.test.models.Market;

public class MarketMapper {

    public static Market proprietaryDataToMarketMapper(String[] tokens) {

        Market market = new Market();
        market.setHeader(HeaderMapper.proprietaryDataToHeaderMapper(tokens));
        market.setEventId(tokens[4]);
        market.setMarketId(tokens[5]);
        market.setName(tokens[6]);
        market.setDisplayed(Boolean.parseBoolean(tokens[7]));
        market.setSuspended(Boolean.parseBoolean(tokens[8]));

        return market;
    }
}
