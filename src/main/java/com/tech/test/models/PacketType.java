package com.tech.test.models;

import java.util.HashMap;
import java.util.Map;

public enum PacketType {
    EVENT("event"), MARKET("market"), OUTCOME("outcome");


    private String value;
    private static Map<String, PacketType> map = new HashMap<>();

    static {
        for (PacketType entry : PacketType.values()) {
            map.put(entry.value, entry);
        }
    }

    PacketType(final String value) {
        this.value = value;
    }

    public static PacketType getType(String type) {

        if (map.containsKey(type)) {
            return map.get(type);
        }

        return null;
    }
}
