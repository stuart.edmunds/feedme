package com.tech.test;

import com.tech.test.services.FeedmeServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class FeedmeApplication implements CommandLineRunner {

    private static Logger LOG = LoggerFactory.getLogger(FeedmeApplication.class);

    private FeedmeServiceImpl feedmeService;

    @Autowired
    public FeedmeApplication(FeedmeServiceImpl feedmeService) {
        this.feedmeService = feedmeService;
    }

	@Override
	public void run(String... args) {
		LOG.info("Starting...");
        feedmeService.getFixtureMessageData();
	}

	/**
	 * Entry point
	 *
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(FeedmeApplication.class, args);
	}
}
