package com.tech.test.services;

import com.tech.test.dtos.FixtureDto;
import com.tech.test.repositories.FixtureRepository;
import com.tech.test.repositories.entries.FixtureEntry;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.core.env.Environment;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@RunWith(MockitoJUnitRunner.class)
public class FeedmeServiceImplTest {

    private static final String EXPECTED_PROPRIETARY_DATA_MESSAGE = "|329|create|event|1556222116206|fe4a1119-b671-43f9-b9b8-77709caf670b|Football|Sky Bet Championship|\\\\|Leeds\\\\| vs \\\\|Hull\\\\||1556222132574|0|1|";

    @Mock
    EventProviderServiceImpl eventProviderService;

    @Mock
    Environment env;

    @Mock
    FixtureRepository repository;

    private FeedmeServiceImpl feedmeService;


    @Before
    public void setUp() throws Exception {
        feedmeService = new FeedmeServiceImpl(eventProviderService, env, repository);
    }

    @Test
    public void getNextEventReturnsExpectedProprietaryDataTest() throws IOException {
        // Arrange
        when(eventProviderService.readNextMessage()).thenReturn(EXPECTED_PROPRIETARY_DATA_MESSAGE);

        // Act
        String proprietaryData = feedmeService.getNextMessage();

        // Assert
        assertEquals(EXPECTED_PROPRIETARY_DATA_MESSAGE, proprietaryData);
    }


    @Test
    public void convertValidEventMessageToJsonTest() throws IOException {
        // Arrange
        String eventMessage="|329|create|event|1556222116206|fe4a1119-b671-43f9-b9b8-77709caf670b|Football|Sky Bet Championship|\\|Leeds\\| vs \\|Hull\\||1556222132574|0|1|";

        String[] tokens = eventMessage.split(FeedmeServiceImpl.SPLIT_PROPRIETARY_MESSAGE_REGEX);
        tokens = Arrays.copyOfRange(tokens, 1, tokens.length);

        // Act
        String json = feedmeService.convertMessageToJSON(tokens);

        // Assert
        assertEquals("{\"header\":{\"msgId\":329,\"operation\":\"create\",\"type\":\"event\",\"timestamp\":1556222116206},\"eventId\":\"fe4a1119-b671-43f9-b9b8-77709caf670b\",\"category\":\"Football\",\"subCategory\":\"Sky Bet Championship\",\"name\":\"\\\\|Leeds\\\\| vs \\\\|Hull\\\\|\",\"startTime\":1556222132574,\"displayed\":false,\"suspended\":false}", json);

        // TODO invalid number of tokens - to many, too few, etc
    }

    @Test
    public void convertValidMarketMessageToJsonTest() throws IOException {

        // Arrange
        String marketMessage="|799|create|market|1556258241430|fb837e98-dc62-48ce-9b8e-d5ed6954a4b0|f9932ed7-44f7-4c59-bb34-8b24a137a2fd|Goal Handicap (-1)|0|1|";

        String[] tokens = marketMessage.split(FeedmeServiceImpl.SPLIT_PROPRIETARY_MESSAGE_REGEX);
        tokens = Arrays.copyOfRange(tokens, 1, tokens.length);

        // Act
        String json = feedmeService.convertMessageToJSON(tokens);

        // Assert
        assertEquals("{\"header\":{\"msgId\":799,\"operation\":\"create\",\"type\":\"market\",\"timestamp\":1556258241430},\"eventId\":\"fb837e98-dc62-48ce-9b8e-d5ed6954a4b0\",\"marketId\":\"f9932ed7-44f7-4c59-bb34-8b24a137a2fd\",\"name\":\"Goal Handicap (-1)\",\"displayed\":false,\"suspended\":false}", json);

        // TODO invalid number of tokens - to many, too few, etc
    }

    @Test
    public void convertValidOutcomeMessageToJsonTest() throws IOException {
        // Arrange
        String outcomeMessage="|993|create|outcome|1556259023291|612c2383-e7c7-4b3c-94c3-6c77bd58bf32|7053ace7-86dd-4a33-9d01-d5e3a69ea1ee|Yes|13/8|0|1|";

        String[] tokens = outcomeMessage.split(FeedmeServiceImpl.SPLIT_PROPRIETARY_MESSAGE_REGEX);
        tokens = Arrays.copyOfRange(tokens, 1, tokens.length);

        // Act
        String json = feedmeService.convertMessageToJSON(tokens);

        // Assert
        assertEquals("{\"header\":{\"msgId\":993,\"operation\":\"create\",\"type\":\"outcome\",\"timestamp\":1556259023291},\"marketId\":\"612c2383-e7c7-4b3c-94c3-6c77bd58bf32\",\"outcomeId\":\"7053ace7-86dd-4a33-9d01-d5e3a69ea1ee\",\"name\":\"Yes\",\"price\":\"13/8\",\"displayed\":false,\"suspended\":false}", json);

        // TODO invalid number of tokens - to many, too few, etc
    }

    @Test
    public void getFixtureMessageDataSingleEventTest() {
        // Arrange
        FeedmeServiceImpl feedmeServiceMock = Mockito.mock(FeedmeServiceImpl.class);

        when(feedmeServiceMock.getNextMessage()).thenReturn("|39|create|event|1556400511692|ff17ce3a-61e2-47e4-ada2-57d536f0c623|Football|Premier League|\\\\|Manchester Utd\\\\| vs \\\\|Manchester City\\\\||1556400536018|0|1|\"")
            .thenReturn( "|40|create|market|1556400511692|ff17ce3a-61e2-47e4-ada2-57d536f0c623|fe2c2f27-a282-463f-8ff4-aff0fa2dd4e6|Full Time Result|0|1|")
            .thenReturn("|41|create|outcome|1556400511692|fe2c2f27-a282-463f-8ff4-aff0fa2dd4e6|181235bc-94ac-4f66-9401-6b9141d1a9dd|\\|Manchester Utd\\||11/8|0|1|")
        .thenReturn(null);

        when(feedmeServiceMock.getFixtureMessageData()).thenCallRealMethod();

        // Act
        feedmeServiceMock.getFixtureMessageData();

        // Assert
        verify(feedmeServiceMock, times(1)).processMessages(anyList());
    }

    @Test
    public void getFixtureMessageDataMultipleEventsTest() {
        // Arrange
        FeedmeServiceImpl feedmeServiceMock = Mockito.mock(FeedmeServiceImpl.class);
        when(feedmeServiceMock.getNextMessage()).thenReturn("\"|39|create|event|1556400511692|ff17ce3a-61e2-47e4-ada2-57d536f0c623|Football|Premier League|\\\\|Manchester Utd\\\\| vs \\\\|Manchester City\\\\||1556400536018|0|1|\"")
            .thenReturn( "|40|create|market|1556400511692|ff17ce3a-61e2-47e4-ada2-57d536f0c623|fe2c2f27-a282-463f-8ff4-aff0fa2dd4e6|Full Time Result|0|1|")
            .thenReturn("|41|create|outcome|1556400511692|fe2c2f27-a282-463f-8ff4-aff0fa2dd4e6|181235bc-94ac-4f66-9401-6b9141d1a9dd|\\|Manchester Utd\\||11/8|0|1|")
            .thenReturn("\"|42|create|event|1556400511692|ff17ce3a-61e2-47e4-ada2-57d536f0c623|Football|Premier League|\\\\|Manchester Utd\\\\| vs \\\\|Manchester City\\\\||1556400536018|0|1|\"")
            .thenReturn( "|43|create|market|1556400511692|ff17ce3a-61e2-47e4-ada2-57d536f0c623|fe2c2f27-a282-463f-8ff4-aff0fa2dd4e6|Full Time Result|0|1|")
            .thenReturn("|44|create|outcome|1556400511692|fe2c2f27-a282-463f-8ff4-aff0fa2dd4e6|181235bc-94ac-4f66-9401-6b9141d1a9dd|\\|Manchester Utd\\||11/8|0|1|")
            .thenReturn("\"|45|create|event|1556400511692|ff17ce3a-61e2-47e4-ada2-57d536f0c623|Football|Premier League|\\\\|Manchester Utd\\\\| vs \\\\|Manchester City\\\\||1556400536018|0|1|\"")
            .thenReturn( "|46|create|market|1556400511692|ff17ce3a-61e2-47e4-ada2-57d536f0c623|fe2c2f27-a282-463f-8ff4-aff0fa2dd4e6|Full Time Result|0|1|")
            .thenReturn("|47|create|outcome|1556400511692|fe2c2f27-a282-463f-8ff4-aff0fa2dd4e6|181235bc-94ac-4f66-9401-6b9141d1a9dd|\\|Manchester Utd\\||11/8|0|1|")
            .thenReturn(null);

        when(feedmeServiceMock.getFixtureMessageData()).thenCallRealMethod();

        doAnswer((fixtureDto) -> {
            assertNotNull(fixtureDto);
            return null; // Return null required because this is a void method
        }).when(feedmeServiceMock).insertFixtureDto(anyObject());

        // Act
        feedmeServiceMock.getFixtureMessageData();

        // Assert
        verify(feedmeServiceMock, times(3)).processMessages(anyList());
    }

    @Test
    public void processMessagesWithSingleMarketAndOutcomeTest() {
        // Arrange
        List<String> fixtureMessagesAsJson = new ArrayList<>();
        fixtureMessagesAsJson.add("{\"header\":{\"msgId\":39,\"operation\":\"create\",\"type\":\"event\",\"timestamp\":1556400511692},\"eventId\":\"ff17ce3a-61e2-47e4-ada2-57d536f0c623\",\"category\":\"Football\",\"subCategory\":\"Premier League\",\"name\":\"\\\\\\\\|Manchester Utd\\\\\\\\| vs \\\\\\\\|Manchester City\\\\\\\\|\",\"startTime\":1556400536018,\"displayed\":false,\"suspended\":false}");
        fixtureMessagesAsJson.add("{\"header\":{\"msgId\":40,\"operation\":\"create\",\"type\":\"market\",\"timestamp\":1556400511692},\"eventId\":\"ff17ce3a-61e2-47e4-ada2-57d536f0c623\",\"marketId\":\"fe2c2f27-a282-463f-8ff4-aff0fa2dd4e6\",\"name\":\"Full Time Result\",\"displayed\":false,\"suspended\":false}");
        fixtureMessagesAsJson.add("{\"header\":{\"msgId\":41,\"operation\":\"create\",\"type\":\"outcome\",\"timestamp\":1556400511692},\"marketId\":\"fe2c2f27-a282-463f-8ff4-aff0fa2dd4e6\",\"outcomeId\":\"181235bc-94ac-4f66-9401-6b9141d1a9dd\",\"name\":\"\\\\|Manchester Utd\\\\|\",\"price\":\"11/8\",\"displayed\":false,\"suspended\":false}");

        // Act
        FixtureDto fixtureDto = feedmeService.processMessages(fixtureMessagesAsJson);

        // Assert
        assertNotNull(fixtureDto);
        assertEquals(1, fixtureDto.getMarketDto().size());
        assertEquals(1, fixtureDto.getMarketDto().get(0).getOutComeDto().size());
    }

    @Test
    public void CreateFixtureEntryFromFixtureDto() {
        // Arrange
        List<String> fixtureMessagesAsJson = new ArrayList<>();
        fixtureMessagesAsJson.add("{\"header\":{\"msgId\":39,\"operation\":\"create\",\"type\":\"event\",\"timestamp\":1556400511692},\"eventId\":\"ff17ce3a-61e2-47e4-ada2-57d536f0c623\",\"category\":\"Football\",\"subCategory\":\"Premier League\",\"name\":\"\\\\\\\\|Manchester Utd\\\\\\\\| vs \\\\\\\\|Manchester City\\\\\\\\|\",\"startTime\":1556400536018,\"displayed\":false,\"suspended\":false}");
        fixtureMessagesAsJson.add("{\"header\":{\"msgId\":40,\"operation\":\"create\",\"type\":\"market\",\"timestamp\":1556400511692},\"eventId\":\"ff17ce3a-61e2-47e4-ada2-57d536f0c623\",\"marketId\":\"fe2c2f27-a282-463f-8ff4-aff0fa2dd4e6\",\"name\":\"Full Time Result\",\"displayed\":false,\"suspended\":false}");
        fixtureMessagesAsJson.add("{\"header\":{\"msgId\":41,\"operation\":\"create\",\"type\":\"outcome\",\"timestamp\":1556400511692},\"marketId\":\"fe2c2f27-a282-463f-8ff4-aff0fa2dd4e6\",\"outcomeId\":\"181235bc-94ac-4f66-9401-6b9141d1a9dd\",\"name\":\"\\\\|Manchester Utd\\\\|\",\"price\":\"11/8\",\"displayed\":false,\"suspended\":false}");

        // Act
        FixtureDto fixtureDto = feedmeService.processMessages(fixtureMessagesAsJson);
        FixtureEntry entry = feedmeService.createFixtureDto(fixtureDto);

        // Assert
        assertEquals("ff17ce3a-61e2-47e4-ada2-57d536f0c623",entry.getEventId());
        assertEquals("Football",entry.getCategory());
        assertEquals("Premier League",entry.getSubCategory());
        assertEquals("\\\\|Manchester Utd\\\\| vs \\\\|Manchester City\\\\|",entry.getName());
        assertEquals(1556400536018l,entry.getStartTime());
        assertEquals(false,entry.isDisplayed());
        assertEquals(false,entry.isSuspended());
        assertNotNull(entry.getMarketDto());
        assertEquals("ff17ce3a-61e2-47e4-ada2-57d536f0c623", entry.getMarketDto().get(0).getEventId());
        assertEquals("fe2c2f27-a282-463f-8ff4-aff0fa2dd4e6", entry.getMarketDto().get(0).getMarketId());
        assertEquals("Full Time Result", entry.getMarketDto().get(0).getName());
        assertEquals(false, entry.getMarketDto().get(0).isDisplayed());
        assertEquals(false, entry.getMarketDto().get(0).isSuspended());
        assertNotNull(entry.getMarketDto().get(0).getOutComeDto());
        assertEquals("fe2c2f27-a282-463f-8ff4-aff0fa2dd4e6", entry.getMarketDto().get(0).getOutComeDto().get(0).getMarketId());
        assertEquals("181235bc-94ac-4f66-9401-6b9141d1a9dd", entry.getMarketDto().get(0).getOutComeDto().get(0).getOutcomeId());
        assertEquals("\\|Manchester Utd\\|", entry.getMarketDto().get(0).getOutComeDto().get(0).getName());
        assertEquals("11/8", entry.getMarketDto().get(0).getOutComeDto().get(0).getPrice());
        assertEquals(false, entry.getMarketDto().get(0).getOutComeDto().get(0).isDisplayed());
        assertEquals(false, entry.getMarketDto().get(0).getOutComeDto().get(0).isSuspended());
    }
}

