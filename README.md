# FeedMe Project

## Technologies Used

  - Springboot Console Application (1.5.20.RELEASE)
  - Java 8
  - Springboot version: 1.5.20.RELEASE
  - Maven use to build 


### Package Structure Overview 
`com.tech.test`
  - Springboot main class location: FeedmeApplication.java 

`com.tech.test.config`
  - Classes here used for setting up any Beans during springboots initialisation

`com.tech.test.dtos`
  - Classes here are used to hold all the event data and the child markets and the outcome for the fixture. These classes are used when persisting to db  

`com.tech.test.models`
  - Classes here are used when converting the proprietary message data into a JSON string

`com.tech.test.repositories`
  - Classes here are required for persisting (JPA) fixtures/events to db

`com.tech.test.services`
  - Classes here provide all the required services 

## How to Build Feedme client

```bash
mvn clean package

```
## How to run on command line

Start the Feedme provider before running the client. Then from project root issue the following command:

```bash
java -jar .\target\feedme-0.0.1-SNAPSHOT.jar
```

Once the application is running data will start to be persisted (within mongodb)

![Compass Example](etc/images/compassExample.png)

Database name: `fixtures` collection name: `fixtureEntry`
